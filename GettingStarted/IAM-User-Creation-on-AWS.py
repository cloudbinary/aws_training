'''
Create an IAM User and Login From AWS CLI from Base Machine(i.e. Windows|MacOS|Linux)

STEP-1 : Login to AWS Account

STEP-2 : Create an IAM User & Grant Access to Programmatic Access & AWS Management Console

STEP-3 : Assign Policies 

STEP-4 : Go to Base Machine and Configure user

STEP-5 : Download, Install & Configure AWS CLI Agent

STEP-6 : Login To AWS Cloud Using CLI

STEP-7 : Validate user 

STEP-8 : CleanUp!

https://youtu.be/9Yqmccu0ghQ

'''