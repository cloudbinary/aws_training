Create an IAM User and Login From AWS CLI from Base Machine(i.e. Windows|MacOS|Linux)

STEP-1 : Login to AWS Account

STEP-2 : Create an IAM User & Grant Access to Programmatic Access & AWS Management Console

STEP-3 : Assign Policies 

STEP-4 : Go to Base Machine and Configure user

STEP-5 : Download, Install & Configure AWS CLI Agent

STEP-6 : Login To AWS Cloud Using CLI

STEP-7 : Validate user 

STEP-8 : Download, Install & Configure Terraform on your base machine i.e. Windows|MacOS|Linux 

https://www.terraform.io/

https://www.terraform.io/downloads.html

STEP-9 : Validate terraform version on your base machine  i.e. Windows|MacOS|Linux 

$ terraform --version 

STEP-10 : Create a folder and create a file and save in .tf extension 

$ mkdir demo-terraform/

$ cd demo-terraform/

$ touch main.tf 

FileName : main.tf 

provider "aws" {
    profile = "joel"
    region = "us-east-1"
}

resource "aws_instance" "demo" {
    ami = "ami-085925f297f89fce1"
    instance_type = "t2.micro"
    key_name = "terraform_aws_keys"
    vpc_security_group_ids = [
        "sg-07e9d91872fbc5bcf",
    ]
    
    tags = {
        Instance_Name   = "demoInstance"
        Environment     = "development"
        Project_id      = 2020
        Project_Name    = "Cloud Binary Cloud Computing Certification Training"
        TeamName        = "Cloud Ops"
        Employee_id     = "cb006611"
        Email_id        = "contact.cloudbinary@gmail.com"
        WhatsApp_Number = 7330937999
        WebSite         = "https://cloudbinary.io/"
    }
  
}

STEP-11 : Execute Terraform Commands 

$ terraform init 

$ terraform plan 

$ terraform apply

STEP-12 : CleanUp!

$ terraform destroy 


Our Website :
https://cloudbinary.io/