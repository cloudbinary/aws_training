provider "aws" {
    profile = "joel"
    region = "us-east-1"
}

resource "aws_instance" "demo" {
    ami = "ami-085925f297f89fce1"
    instance_type = "t2.micro"
    key_name = "terraform_aws_keys"
    vpc_security_group_ids = [
        "sg-07e9d91872fbc5bcf",
    ]
    
    tags = {
        Instance_Name   = "demoInstance"
        Environment     = "development"
        Project_id      = 2020
        Project_Name    = "Cloud Binary Cloud Computing Certification Training"
        TeamName        = "Cloud Ops"
        Employee_id     = "cb006611"
        Email_id        = "contact.cloudbinary@gmail.com"
        WhatsApp_Number = 7330937999
        WebSite         = "https://cloudbinary.io/"
    }
  
}
